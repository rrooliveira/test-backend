CREATE DATABASE akna;

CREATE TABLE `akna`.`lista_alimentos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mes` VARCHAR(45) NOT NULL,
  `id_categoria` int NOT NULL,
  `produto` VARCHAR(255) NOT NULL,
  `quantidade` INT NOT NULL,
  `data_cadastro` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));

CREATE TABLE `akna`.`categorias` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(255) NOT NULL,
  `data_cadastro` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));

ALTER TABLE `akna`.`lista_alimentos`
ADD CONSTRAINT `fk_categoria`
  FOREIGN KEY (`id_categoria`)
  REFERENCES `akna`.`categorias` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

INSERT INTO akna.categorias (descricao) values ('Alimentos');
INSERT INTO akna.categorias (descricao) values ('Higiene Pessoal');
INSERT INTO akna.categorias (descricao) values ('Limpeza');
