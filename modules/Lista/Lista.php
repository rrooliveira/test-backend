<?php

/**
 * Class Lista
 * Desenvolvedor: Rodrigo Ruy Oliveira
 * Contato: rro.oliveira@gmail.com
 */
class Lista
{
    private $db = null;
    private $listaAlimentos = null;
    private $listaAlimentosOrdenada = null;
    private $categorias = null;
    private $produtos = null;

    private function __construct()
    {
        $this->db = Database::getInstance();
    }

    public static function getInstance()
    {
        static $instance = null;

        if($instance === null){
            $instance = new Lista();
        }
        return $instance;
    }

    public function setListaAlimentos($listaAlimentos)
    {
        try {
            if (is_array($listaAlimentos) && !is_null($listaAlimentos)) {
                $this->listaAlimentos = $listaAlimentos;
                $this->ordenarListaAlimentos();
            } else {
                throw new Exception('Informe uma lista de alimentos');
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getListaAlimentosOrdenada()
    {
        if (!is_array($this->listaAlimentosOrdenada)) {
            return 'A lista de alimentos ainda não foi ordenada';
        } else {
            return $this->listaAlimentosOrdenada;
        }
    }

    private function ordenarListaAlimentos()
    {
        try {
            if (is_array($this->listaAlimentos) && !is_null($this->listaAlimentos)) {
                $this->ordenarPorMes();

                foreach ($this->listaAlimentos as $mes => $categorias) {
                    $this->categorias = $categorias;
                    $this->ordenarPorCategoria();

                    foreach ($this->categorias as $categoria => $produtos) {
                        $this->produtos = $produtos;
                        $this->ordenarPorQuantidade();

                        foreach ($this->produtos as $produto => $quantidade) {
                            //Ajustar nome das categorias
                            $categoria = str_replace('_', ' ', $categoria);

                            //Ajustar o mês de março
                            if ($mes == 'marco') $mes = 'março';

                            //Corrigir palavras
                            $produto = $this->corrigirPalavras($produto);

                            $this->listaAlimentosOrdenada[$mes][$categoria][$produto] = $quantidade;
                        }
                    }
                }
            } else {
                throw new Exception('Utilize o método setListaAlimentos para carregar a lista de alimentos.');
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function ordenarPorMes()
    {
        try {
            if (is_array($this->listaAlimentos) && !is_null($this->listaAlimentos)) {
                uksort($this->listaAlimentos, 'ordenarPorMes');
            } else {
                throw new Exception('Utilize o método setListaAlimentos para carregar a lista de alimentos.');
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function ordenarPorCategoria()
    {
        if (is_array($this->categorias) && !is_null($this->categorias)) {
            uksort($this->categorias, 'strcmp');
        }
    }

    private function ordenarPorQuantidade()
    {
        if (is_array($this->produtos) && !is_null($this->produtos)) {
            arsort($this->produtos);
        }
    }

    private function corrigirPalavras($palavra)
    {
        $arrayPalavrasErradas = [
            'Papel Hignico',
            'Brocolis',
            'Chocolate ao leit',
            'Sabao em po',
            'Geléria de morango'
        ];
        $arrayPalavrasCertas = [
            'Papel Higiênico',
            'Brócolis',
            'Chocolate ao leite',
            'Sabão em pó',
            'Geléia de morango'
        ];

        return str_replace($arrayPalavrasErradas, $arrayPalavrasCertas, $palavra);
    }

    public function gerarListaCsv()
    {
        $fp = fopen('./arquivos_gerados/arquivo.csv', 'w');
        fputcsv($fp, array('Mês', 'Categoria', 'Produto', 'Quantidade'));

        foreach($this->listaAlimentosOrdenada as $mes => $categorias){
            foreach($categorias as $categoria => $produtos){
                foreach($produtos as $produto => $quantidade) {
                    fputcsv($fp, array(ucwords($mes), ucwords($categoria), ucwords($produto), $quantidade));
                }
            }
        }
        fclose($fp);
    }

    public function gravarListaAlimentos()
    {
        try {
            if (is_array($this->listaAlimentosOrdenada) && !is_null($this->listaAlimentosOrdenada)) {
                foreach ($this->listaAlimentosOrdenada as $mes => $categorias) {
                    foreach ($categorias as $categoria => $produtos) {
                        foreach ($produtos as $produto => $quantidade) {

                            //Obter o id da categoria
                            $objCategoria = Categoria::getInstance();
                            $dadosCategoria = $objCategoria->getCategoriaPorNome(ucwords($categoria));

                            if(!is_array($dadosCategoria)) throw new Exception('Categoria não cadastrada - ' . $categoria);

                            $sql = 'INSERT INTO lista_alimentos (mes, id_categoria, produto, quantidade) VALUES (:mes, :id_categoria, :produto, :quantidade)';
                            $stmt = $this->db->prepare($sql);
                            $stmt->bindValue(':mes', ucwords($mes));
                            $stmt->bindValue(':id_categoria', $dadosCategoria['id']);
                            $stmt->bindValue(':produto', ucwords($produto));
                            $stmt->bindValue(':quantidade', $quantidade);
                            $stmt->execute();
                        }
                    }
                }
            } else {
                return 'A lista de alimentos ainda não foi ordenada';
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}