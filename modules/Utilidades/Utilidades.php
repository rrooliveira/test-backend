<?php

function ordenarPorMes($a, $b)
{
    $meses = array(
        'janeiro',
        'fevereiro',
        'marco',
        'abril',
        'maio',
        'junho',
        'julho',
        'agosto',
        'setembro',
        'outubro',
        'novembro',
        'dezembro'
    );

    if (array_search($a, $meses) == array_search($b, $meses)) {
        return 0;
    }
    return array_search($a, $meses) > array_search($b, $meses) ? 1 : -1;
}

function ordenarPorCategoria($a, $b)
{
    $categoria = array(
        'alimentos',
        'higiene_pessoal',
        'limpeza'
    );

    if (array_search($a, $categoria) == array_search($b, $categoria)) {
        return 0;
    }
    return array_search($a, $categoria) > array_search($b, $categoria) ? 1 : -1;
}