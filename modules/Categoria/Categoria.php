<?php

/**
 * Class Categoria
 * Desenvolvedor: Rodrigo Ruy Oliveira
 * Contato: rro.oliveira@gmail.com
 */
class Categoria
{
    private $db = null;

    private function __construct()
    {
        $this->db = Database::getInstance();
    }

    public static function getInstance()
    {
        static $instance = null;

        if($instance === null){
            $instance = new Categoria();
        }
        return $instance;
    }

    public function getCategoriaPorNome(string $descricao)
    {
        try {
            if(!is_string($descricao) || is_null($descricao)) throw new Exception('Informe a descrição da categoria.');

            $array = [];
            $sql = $this->db->prepare('SELECT id FROM categorias WHERE descricao = :descricao');
            $sql->bindValue(":descricao", $descricao);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $array = $sql->fetch();
            }
            return $array;
        } catch (Exception $e) {
            return $e;
        }
    }
}