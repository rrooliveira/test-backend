<?php
$this->get('lista', function ($arg) {
    $template = $this->core->loadModule('template');
    $template->render('lista_tpl', []);
});

$this->get('lista-csv', function ($arg) {
    $listaAlimentos = require './lista-de-compras.php';
    $template = $this->core->loadModule('template');

    $objLista = $this->core->loadModule('lista');
    $objLista->setListaAlimentos($listaAlimentos);
    $objLista->gerarListaCsv();

    $template->render('lista_csv_tpl', $array);

    $arquivo =  './arquivos_gerados/arquivo.csv';
    if (file_exists($arquivo)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($arquivo));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($arquivo));
        ob_clean();
        flush();
        readfile($arquivo);
        exit;
    }
});

$this->get('lista-bd', function ($arg) {
    $listaAlimentos = require './lista-de-compras.php';

    $template = $this->core->loadModule('template');

    $objLista = $this->core->loadModule('lista');
    $objLista->setListaAlimentos($listaAlimentos);
    $objLista->gravarListaAlimentos();

    $array['lista'] = $objLista->getListaAlimentosOrdenada();

    $template->render('lista_bd_tpl', $array);
});

$this->get('lista/{id}', function ($arg) {
    $template = $this->core->loadModule('template');
    $news = $this->core->loadModule('news');

    $array['new'] = $news->getNew($arg['id']);

    $template->render('noticia_tpl', $array);
});